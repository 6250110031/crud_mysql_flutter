<?php

	$host="localhost";      //replace with your hostname 
	$username="root";       //replace with your username 
	$password="";           //replace with your password 
	$db_name="android";     //replace with your database 

    //open connection to mysql db
    $connection = new mysqli($host,$username,$password,$db_name);
	if($connection->error) {
		die("Error Connection failed " .$connection->error);
	}
	if(!$connection->set_charset("utf8")) {  
		printf("Error loading character set utf8: %s\n", $connection->error);  
		exit();  
	}
	
    //fetch table rows from mysql db
    $sql = "select * from products";
    $result = $connection->query($sql); 

    //create products array
    $prodarray = array();
    while($row =$result->fetch_assoc())
    {
        //add MySQL result to products array
		$prodarray[] = $row;
    }
	
	//set http header with content type as JSON and encoded with utf-8
	header("Content-Type: application/json;charset=utf-8");
	
	//Convert PHP array to JSON String
    echo json_encode($prodarray);
	
	//Makes the JSON easily readable
	//echo json_encode($prodarray, JSON_PRETTY_PRINT);
	
	//display utf-8 character in output instead of \u - encoded string
    //echo json_encode($prodarray, JSON_UNESCAPED_UNICODE);

    //close the db connection
    $connection->close();
?>